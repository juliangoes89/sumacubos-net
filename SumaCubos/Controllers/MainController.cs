﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SumaCubos.Helpers;
using SumaCubos.Models;

namespace SumaCubos.Controllers
{
	public class MainController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Index(int T,string Commands)
		{
			var result = "";
			Input Input = InputProcessor.CreateInputCases(T,Commands);
			var ValidationErrors = InputProcessor.Validate(Input);
			if (ValidationErrors.Count == 0)
			{
				result = InputProcessor.ProcessCases(Input);
			}
			else {
				foreach (var error in ValidationErrors)
				{
					result += error.ErrorMessages;
				}
			}
			ViewBag.result = result;
			return View();
		}

	}
}