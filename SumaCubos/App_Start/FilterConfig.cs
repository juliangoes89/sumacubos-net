﻿using System.Web;
using System.Web.Mvc;
using SumaCubos.Models;

namespace SumaCubos
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
			
		}
	}
}
