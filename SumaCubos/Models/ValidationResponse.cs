﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SumaCubos.Models
{
	public class ValidationResponse
	{
		public bool Success { get; set; }
		public List<String> ErrorMessages { get; set; }
	}
}