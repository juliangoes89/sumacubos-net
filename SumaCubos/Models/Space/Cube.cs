﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SumaCubos.Models
{
	public class Cube
	{
		public int x { get; set; }
		public int y { get; set; }
		public int z { get; set; }
		public int val { get; set; }
	}
}