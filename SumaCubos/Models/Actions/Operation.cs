﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SumaCubos.Models
{
	//public enum OperationType
	//{
		//Query,
		//Update
	//}
	public abstract class Operation
	{
		//public OperationType OperationType { get; set; }
		public string Key { get; set; }
		public string Name { get; set; }
		public List<int> Parameters { get; set; }
	}
	public class Query : Operation
	{
		public Query() {
			this.Key = "Q";
			this.Name = "QUERY";
			this.Parameters = new List<int>(6); //x1 y1 z1 x2 y2 z2
		}

	}
	public class Update: Operation
	{
		public Update() {
		this.Key = "U";
		this.Name = "UPDATE";
		this.Parameters = new List<int>(4); // x y z W
		}
	}
}