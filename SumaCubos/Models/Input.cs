﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SumaCubos.Models
{
	public class Input
	{
		[Required(ErrorMessage = "El número de Casos de prueba es obligatorio")]
		[Range(1, 50, ErrorMessage = "El número de Casos de prueba debe estar entre 1 y 50")]
		public int T { get; set; } //Test Cases
		public List<InputCase> inputCases { get; set; }
	}
}