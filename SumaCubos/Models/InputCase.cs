﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SumaCubos.Models
{
	public class InputCase
	{

		[Required(ErrorMessage = "El tamaño de la matriz es obligatorio")]
		[Range(1, 100, ErrorMessage = "El tamaño de la matriz debe ser entre 1 y 100")]
		public int N { get; set; } //Matrix Lenght N*N*N
		[Required(ErrorMessage = "El numero de operaciones es obligatorio")]
		[Range(1, 1000, ErrorMessage = "El numero de operaciones debe ser entre 1 y 1000")]
		public int M { get; set; } //Number Of Operations
		public List<Operation> Operations { get; set; } 

	}
}