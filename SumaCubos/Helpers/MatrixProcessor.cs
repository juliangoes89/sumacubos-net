﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SumaCubos.Models;

namespace SumaCubos.Helpers
{
	public class MatrixProcessor
	{
		public List<Matrix> Matrices { get; set; } = new List<Matrix>();

		public void ConstruirMatrices(List<int> Ns) {
			foreach (var N in Ns)
			{
				var _matriz =ConstruirMatriz(N);
				Matrices.Add(_matriz);
			}
		}

		public Matrix ConstruirMatriz(int N) {
			Matrix matriz = new Matrix();
			matriz.N = N;
			Cube[][][] cubos = new Cube[N][][];
			for (int i = 0; i < cubos.Length; i++)
			{ //to construct 3D array
				cubos[i] = new Cube[N][];
				for (int j = 0; j < cubos[i].Length; j++)
					cubos[i][j] = new Cube[N];
			}

			for (int i = 0; i < N; i++)
			{
				for (int j = 0; j < N; j++)
				{
					for (int k = 0; k < N; k++)
					{
						Cube cubo = new Cube();
						cubo.x = i;
						cubo.y = j;
						cubo.z = k;
						cubo.val = 0;
						cubos[i][j][k] = cubo;
					}
				}
			}
			matriz.Cubes = cubos;
			Matrices.Add(matriz);
			return matriz;
		}

		public Cube Update(int CaseNumber,Cube cube) {
			Matrix Matriz = Matrices[CaseNumber];
			var i = cube.x;
			var j = cube.y;
			var k = cube.z;
			Matriz.Cubes[i][j][k] = cube;
			return cube;
		}
		public int Query(int CaseNumber,Cube minCube, Cube maxCube) {
			int result = 0;
			Matrix Matriz = Matrices[CaseNumber];
			var x1 = minCube.x;
			var y1 = minCube.y;
			var z1 = minCube.z;
			var x2 = maxCube.x;
			var y2 = maxCube.y;
			var z2 = maxCube.z;
			for (int i = x1; i <= x2; i++)
			{
				for (int j = y1; j <= y2; j++)
				{
					for (int k = z1; k <= z2; k++)
					{
						result += Matriz.Cubes[i][j][k].val;
					}
				}
			}
			return result;
		}
	}
}