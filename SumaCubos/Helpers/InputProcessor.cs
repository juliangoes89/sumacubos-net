﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SumaCubos.Models;

namespace SumaCubos.Helpers
{
	public class InputProcessor
	{
		public static Input CreateInputCases(int T, string sInput) {
			Input _input = new Input();
			_input.T = T;
			_input.inputCases = new List<InputCase>();
			string[] inputLines = sInput.Split(new[] { "\r\n", "\r", "\n" },StringSplitOptions.None);
			InputCase currentCase = null;
			for (int i = 1; i < inputLines.Length; i++)
			{
				if (!(inputLines[i].Contains("UPDATE") || inputLines[i].Contains("QUERY")))
				{
					InputCase _case = new InputCase();
					_case.Operations = new List<Operation>();
					_case.N = int.Parse(inputLines[i].Split(null)[0]);
					_case.M = int.Parse(inputLines[i].Split(null)[1]);
					currentCase = _case;
				}
				else
				{
					for (int j = i; j < currentCase.M + i; j++)
					{
						if (inputLines[j].Contains("UPDATE"))
						{
							Update update = new Update();
							int x = int.Parse(inputLines[j].Split(null)[1])-1;
							int y = int.Parse(inputLines[j].Split(null)[2])-1;
							int z = int.Parse(inputLines[j].Split(null)[3])-1;
							int W = int.Parse(inputLines[j].Split(null)[4]);
							update.Parameters = new List<int> { x, y, z, W };
							currentCase.Operations.Add(update);
						}
						else if (inputLines[j].Contains("QUERY"))
						{

							Query query = new Query();

							int x1 = int.Parse(inputLines[j].Split(null)[1])-1;
							int y1 = int.Parse(inputLines[j].Split(null)[2])-1;
							int z1 = int.Parse(inputLines[j].Split(null)[3])-1;
							int x2 = int.Parse(inputLines[j].Split(null)[4])-1;
							int y2 = int.Parse(inputLines[j].Split(null)[5])-1;
							int z2 = int.Parse(inputLines[j].Split(null)[6])-1;
							query.Parameters = new List<int> { x1, y1, z1, x2, y2, z2 };
							currentCase.Operations.Add(query);
						}
					}
					i = i + (currentCase.M - 1);
					_input.inputCases.Add(currentCase);
				}
				
			}
			return _input;
		}
		public static List<ValidationResponse> Validate(Input input) {
			List<ValidationResponse> validations = new List<ValidationResponse>();
			List<InputCase> inputCases = input.inputCases;
			foreach (var inputCase in inputCases)
			{
				var validation = new ValidationResponse();
				validation.Success = true;
				validation.ErrorMessages = new List<string>();
				if (!((1 <= inputCase.N) && (inputCase.N <= 100)))
				{
					validation.ErrorMessages.Add("El tamaño de la matriz debe ser entre 1 y 100");
					validation.Success = false;
				}
				if (!((1 <= inputCase.M) && (inputCase.M <= 1000)))
				{
					validation.ErrorMessages.Add("El numero de operaciones debe ser entre 1 y 1000");
					validation.Success = false;
				}
				else
				{
					if (inputCase.M != inputCase.Operations.Count)
					{
						validation.ErrorMessages.Add("El numero de operaciones M y la Cantidad de Operaciones asociadas no coinciden");
						validation.Success = false;
					}
				}
				if (!validation.Success) {
					validations.Add(validation);
				}
			}
			return validations;
		}
		public static string ProcessCases(Input input) {
			string result = "";
			int CaseNumber = 0;
			MatrixProcessor matrixP = new MatrixProcessor();
			foreach (var inputCase in input.inputCases)
			{
				var N = inputCase.N;
				Matrix matriz =matrixP.ConstruirMatriz(N);
				result += ProcessOperations(inputCase,CaseNumber,matriz,matrixP);
				CaseNumber++;
			}
			return result;
		}
		public static string ProcessOperations(InputCase inputCase,int CaseNumber,Matrix matrix,MatrixProcessor matrixP) {
			var Operations = inputCase.Operations;
			string OutputLog = "";
			foreach (var operation in Operations)
			{
				var key = operation.Key;
				if (key == "U")
				{
					Cube cube = new Cube();
					cube.x = operation.Parameters[0];
					cube.y = operation.Parameters[1];
					cube.z = operation.Parameters[2];
					cube.val = operation.Parameters[3];
					cube = matrixP.Update(CaseNumber, cube);
					//OutputLog += "Case Number:"+CaseNumber+"UPDATE x:"+cube.x +" y:"+cube.y +" z:"+cube.z +" value:"+cube.val +"\r\n";
				}
				else
				{
					if (key=="Q") {
						Cube minCube = new Cube();
						minCube.x = operation.Parameters[0];
						minCube.y = operation.Parameters[1];
						minCube.z = operation.Parameters[2];
						Cube maxCube = new Cube();
						maxCube.x = operation.Parameters[3];
						maxCube.y = operation.Parameters[4];
						maxCube.z = operation.Parameters[5];
						var result = 0;
						result = matrixP.Query(CaseNumber,minCube,maxCube);
						//OutputLog += "Case Number:" + CaseNumber + "QUERY:"+result+"\r\n";
						OutputLog += result + "\r\n";
					}
				}
			}
			return OutputLog;
		}
	}
}